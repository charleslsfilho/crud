package com.cms.crud.service;

import com.cms.crud.Exception.ResourceNotFoundExeception;
import com.cms.crud.data.vo.ProdutoVO;
import com.cms.crud.entity.Produto;
import com.cms.crud.message.ProdutoSendMessage;
import com.cms.crud.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProdutoService {

    private final ProdutoRepository repository;

    private final ProdutoSendMessage produtoSendMessage;

    @Autowired
    public ProdutoService(ProdutoRepository repository, ProdutoSendMessage produtoSendMessage) {
        this.repository = repository;
        this.produtoSendMessage = produtoSendMessage;
    }

    public ProdutoVO create(ProdutoVO produtoVO) {
        ProdutoVO newProdutoVO = ProdutoVO.create(repository.save(Produto.create(produtoVO)));
        produtoSendMessage.sendMessage(newProdutoVO);
        return newProdutoVO;
    }

    public Page<ProdutoVO> findAll(Pageable pageable) {
        var page = repository.findAll(pageable);
        return page.map(this::convertToProdutoVO);
    }

    private ProdutoVO convertToProdutoVO(Produto produto) {
        return ProdutoVO.create(produto);
    }

    public ProdutoVO findById(Long id) {
        var entity = repository.findById(id).orElseThrow(() -> new ResourceNotFoundExeception("no records found for this ID " + id));
        return ProdutoVO.create(entity);
    }

    public ProdutoVO update(ProdutoVO produtoVO) {
        repository.findById(produtoVO.getId()).orElseThrow(() -> new ResourceNotFoundExeception("no records found for this ID " + produtoVO.getId()));
        return ProdutoVO.create(repository.save(Produto.create(produtoVO)));
    }

    public void delete(Long id) {
        var entity = repository.findById(id).orElseThrow(() -> new ResourceNotFoundExeception("no records found for this ID " + id));
        repository.delete(entity);
    }
}
